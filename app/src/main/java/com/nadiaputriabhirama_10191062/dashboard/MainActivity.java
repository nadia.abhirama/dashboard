package com.nadiaputriabhirama_10191062.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        String[] texts = new String[]{
                "My Account",
                "Tasks",
                "Favorites",
                "Messages"
        };

        int[] images = new int[]{
                R.drawable.ic_baseline_account_box_24,
                R.drawable.ic_baseline_done_all_24,
                R.drawable.ic_baseline_favorite_24,
                R.drawable.ic_baseline_chat_24
        };

        RvAdapter adapter = new RvAdapter(this, images, texts);

        rv = findViewById(R.id.rv_menu);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this, 2));

    }
}